# README #

This is the work done in GSoC17 for Red Hen Labs. Repository imported from [mehul_bhatt/gsoc17-krish](https://bitbucket.org/mehul_bhatt/gsoc17-krish)

### Contents of  project ###

* `Working_Vids` contains the videos on which analysis was done. (only audio is in the repo)
* `TUT-SED-synthetic-2016` - Dataset for SED task
* `audio_code` contains most of the code written
    * `sed` contains the code relevant to 'Sound Event Detection'
* Further information given below.

### audio_code ###

* `audio_api.py` is the main file, which generates the csv output.
* Outputs (for each time-frame):
    * Silence vs non silent 
    * Music vs speech segments
    * SpeakerID 
    * Arousal and Valence scores
* Path to the video needs to be given as command line argument when running the file.
    * `python audio_api.py <path_to_video>`
* Dependencies
    * numpy.
    * repo contains PyAudioAnalysis source files.
* `sed` contains work related to the Sound Event Detection tasks

### sed ###

* This is an implementation of the [paper](https://arxiv.org/abs/1604.00861) RNN for Polyphonic SED.
* This is still a WIP. The results obtained are sub-optimal. It will be fixed in upcoming weeks.
* `Model` folders contain the experiment setup
    * `sed_network.py` contains the network class
    * `sed_train_val.py` contains the training and validation codes
* Dataset being used is `TUT-SED-synthetic-2016` which is mentioned in the parent repo.
* Dependencies
    * PyTorch.
    * repo contains DCASE-framework files.
* `weights` of the Models aren't uploaded due to size constraints, please open an Issue in case you want it :)




