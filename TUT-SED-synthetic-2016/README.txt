TUT-SED-synthetic-2016 dataset metadata
=============================================
(Emre Cakir, emre.cakir@tut.fi)

This folder includes the data distribution and the annotations for each mixture in TUT-SED-synthetic-2016.

- meta.txt includes the onset-offset annotations for all the 100 mixtures in the dataset.
- meta/$FILE_NAME.txt includes the onset-offset annotations for each mixture with the name $FILE_NAME.
- evaluation_setup/evaluate.txt includes the onset-offset annotations for all the 20 mixtures in the evaluation (test) set.
- evaluation_setup/test.txt includes the paths for the 20 mixtures in the evaluation (test) set (without annotations).
- evaluation_setup/train.txt includes the onset-offset annotations for all the 60 mixtures in the training set.
- evaluation_setup/validate.txt includes the onset-offset annotations for all the 20 mixtures in the validation set.
- evaluation_setup/train+validate.txt includes the onset-offset annotations for all the 80 mixtures in the training + validation set.
