import json
import os
import csv
import sys
import numpy
import subprocess
import audioSegmentation as aS
import audioBasicIO as aIO
import audioFeatureExtraction as aF
import audioTrainTest as aT
from audioSegmentation import flags2segs, segs2flags

###### Functions ######
def make_dir(folder):
	if not os.path.exists(folder): 
		os.makedirs(folder)

def convert_video(input_vid, output_wav):
	'''
	Args:

		1. path to the video file
		2. path to the output audio

	'''

	subprocess.call("ffmpeg -i {} -vn -acodec pcm_s16le -ar 44100 -ac 2 {}".format(input_vid,output_wav), shell=True)	

def silence_segments(output_wav,result_save=True):
	
	'''
	Args:

		x: 	Input Audio Signal
		Fs: sampling freq

	'''
	[Fs, x] = aIO.readAudioFile(output_wav)
	segments = aS.silenceRemoval(x, Fs, 0.020, 0.020, smoothWindow = 0.8, Weight = 0.3, plot = False)

	if result_save:

		filename = "{}.csv".format(output_wav.split('/')[-1].split('.')[0])
		with open(filename, 'a') as csvfile:
			spamwriter = csv.writer(csvfile, delimiter=' ',quotechar='|', quoting=csv.QUOTE_MINIMAL)
			spamwriter.writerow(["Non-Silent segments: ", segments])

	flagsInd = numpy.ones((len(x)/Fs))
	for segment in segments:
		segment = [int(element) for element in segment]
		flagsInd[segment[0]:segment[1]] = 0.

	return flagsInd

def music_speech(input_file, model_file, gt_file='', result_save=True):
	
	'''
	'''
	[flagsInd, classesAll, acc, CM] = aS.mtFileClassification(input_file, model_file , "svm", False, gt_file)

	(segs, classes) = flags2segs(flagsInd,1)

	music_segments = []
	speech_segments = []

	for i,classlabel in enumerate(classes):
		if classlabel:
			music_segments.append(segs[i])
		else:
			speech_segments.append(segs[i])

	if result_save:

		filename = "{}.csv".format(output_wav.split('/')[-1].split('.')[0])
		with open(filename, 'a') as csvfile:
			spamwriter = csv.writer(csvfile, delimiter=' ',quotechar='|', quoting=csv.QUOTE_MINIMAL)
			spamwriter.writerow(["Music segments: ", music_segments])
			spamwriter.writerow(["Speech segments: ", speech_segments])

	return flagsInd, (1-flagsInd)

def diarization_segments(diar_input, numSpeakers, result_save=True):
	
	speaker_flags = aS.speakerDiarization(diar_input, numSpeakers, LDAdim=0, PLOT=True)
	(segs, classes) = flags2segs(speaker_flags,1)

	speaker_segments = {}
	for i in xrange(numSpeakers):
		speaker_segments[str(i)] = []
	for i,seg in enumerate(segs):
		speaker_segments[str(int(classes[i]))].append(seg)

	if result_save:

		filename = "{}.csv".format(output_wav.split('/')[-1].split('.')[0])
		with open(filename, 'a') as csvfile:
			spamwriter = csv.writer(csvfile, delimiter=' ',quotechar='|', quoting=csv.QUOTE_MINIMAL)
			for key in speaker_segments.keys():
				spamwriter.writerow(["Diarization segments: Speaker {}".format(key), speaker_segments[key]])

	return speaker_flags
	
def arousal_valence(output_wav, result_save=True):

	values, classes = aT.fileRegression(output_wav, "data/svmSpeechEmotion", "svm")

	if result_save:

		filename = "{}.csv".format(output_wav.split('/')[-1].split('.')[0])
		with open(filename, 'a') as csvfile:
			spamwriter = csv.writer(csvfile, delimiter=' ',quotechar='|', quoting=csv.QUOTE_MINIMAL)
			spamwriter.writerow(["Arousal, Valence: ", values[0], values[1] ])

	return values[0], values[1]

def make_csv():

	print "Getting non-silent segments"
	output_wav = "wav/The_Grand_Budapest_Hotel_2014.wav"
	silence = silence_segments(output_wav, False)

	print "Getting music and speech segments"
	input_file = "wav/The_Grand_Budapest_Hotel_2014.wav"
	model_file = "data/svmSM"
	# gt_file = '/media/zoe/Personal/soc/pyAudioAnalysis/file-2.segments'
	gt_file = ""	
	music, speech = music_speech(input_file, model_file, gt_file, False)

	print "Getting speaker segments"
	diar_input =  "wav/The_Grand_Budapest_Hotel_2014.wav"
	numSpeakers = 4
	speakerID = diarization_segments(diar_input, numSpeakers, False)	

	print "Getting arousal and valence"
	arva_input = "wav/The_Grand_Budapest_Hotel_2014.wav"
	arousal, valence = arousal_valence(arva_input, False)


	filename = "results.csv"
	with open(filename, 'wb') as csvfile:
		spamwriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
		spamwriter.writerow(["Time (In Seconds)", "Silence", "Music", "Speech", "SpeakerID", "Arousal (of the entire clip)", "Valence (of the entire clip)", "Events associatedhyd" ])
		spamwriter.writerow(["-", "-", "-", "-", "4 speakers", arousal, valence ])
		spamwriter.writerow([" ", " ", " ", " ", " ", " ", " " ])
		for i in xrange(len(silence)):
			spamwriter.writerow([i, silence[i], music[i], speech[i], speakerID[i]])



###### Paths #######
input_vid = sys.argv[1]
wav_folder = "wav/" 															# Folder where you want the converted wav to be stored
output_wav = wav_folder + input_vid.split('/')[-1].split('.')[0] + ".wav"		# Output wav file name


if __name__ == '__main__':

	make_dir(wav_folder)
	# convert_video(input_vid)

	# print "Getting non-silent segments"
	# output_wav = "wav/The_Grand_Budapest_Hotel_2014.wav"
	# silence_segments(output_wav, True)

	# print "Getting music and speech segments"
	# input_file = "wav/The_Grand_Budapest_Hotel_2014.wav"
	# model_file = "data/svmSM"
	# # gt_file = '/media/zoe/Personal/soc/pyAudioAnalysis/file-2.segments'
	# gt_file = ""	
	# music_speech(input_file, model_file, gt_file, True)

	# print "Getting speaker segments"
	# diar_input =  "wav/The_Grand_Budapest_Hotel_2014.wav"
	# numSpeakers = 4
	# diarization_segments(diar_input, numSpeakers, True)
	
	make_csv()