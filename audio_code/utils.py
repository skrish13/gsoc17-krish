def report_information(non_silent_segments, music_segments, speech_segments):
	
	fileobj = open('report.csv', 'w')
	fileobj.write("Non-Silent Segments:")
	fileobj.write("\n")
	fileobj.write(non_silent_segments)

	fileobj.write("Music Segments:")
	fileobj.write("\n")
	fileobj.write(music_segments)

	fileobj.write("Speech Segments:")
	fileobj.write("\n")
	fileobj.write(speech_segments)


