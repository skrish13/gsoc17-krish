import torch
import torch.nn as nn
import torch.nn.functional as F

class nnet(nn.Module):
	
	def __init__(self, n_feat=256, pooling=[2,2,2], seq_len=2.56, frame_len=0.04, num_classes=16):
		super(nnet, self).__init__()
		
		self.n_feat = n_feat
		output_dim = 40/(pooling[0]*pooling[1]*pooling[2])
		num_frames = int(seq_len/frame_len)
		num_units = int(self.n_feat * output_dim * seq_len * 50)

		self.test = nn.Conv2d(1,self.n_feat,5,padding=(5/2))

		self.features = nn.Sequential(
			nn.Conv2d(1,self.n_feat,5,padding=(5/2)),
			nn.BatchNorm2d(self.n_feat),
			nn.ReLU(),
			nn.Dropout(0.25),
			nn.MaxPool2d((pooling[0],1)),		

			nn.Conv2d(self.n_feat,self.n_feat,5,padding=(5/2)),
			nn.BatchNorm2d(self.n_feat),
			nn.ReLU(),
			nn.Dropout(0.25),
			nn.MaxPool2d((pooling[1],1)),

			nn.Conv2d(self.n_feat,self.n_feat,5,padding=(5/2)),
			nn.BatchNorm2d(self.n_feat),
			nn.ReLU(),
			nn.Dropout(0.25),
			nn.MaxPool2d((pooling[2],1))
		)

		# self.rnn = nn.GRU(,256,1,True)

		self.fnn = nn.Sequential(
			# nn.Linear(num_units, num_units),
			# nn.Sigmoid(),
			# nn.Linear(num_units, num_units),
			# nn.Sigmoid(),
			nn.Linear(num_units, num_frames*num_classes),
			nn.Sigmoid()
		)

		for m in self.modules():
			if isinstance(m, nn.Conv2d):
				n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
				m.weight.data.normal_(0, math.sqrt(2. / n))
			elif isinstance(m, nn.BatchNorm2d):
				m.weight.data.fill_(1)
				m.bias.data.zero_()

	def forward(self, x):

		#-------- CNN --------#
		x = self.features(x)

		#-------- RNN --------#
		# x = x.view(x.size()[0], x.size()[1]*x.size()[2], x.size()[3] )
		# x = self.rnn(x)

		#-------- FNN ---------#
		x = x.view(x.size()[0],-1)
		x = self.fnn(x)

		return x