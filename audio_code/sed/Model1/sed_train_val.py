import csv, pickle
from sed_network import *
from torch.autograd import Variable
import torch.optim as optim
import numpy as np
from dcase_framework.metadata import MetaDataContainer
from sklearn.metrics import classification_report, f1_score
import shutil
import visdom

###### PATHS ######
norm_features_path = "../TUT-SED-synthetic-2016/features_normalized/"
annotation_path = "../TUT-SED-synthetic-2016/meta/"
train_txt = "../TUT-SED-synthetic-2016/evaluation_setup/train.txt"


#### PARAMS ####
epochs = 200
num_classes = 16
classes = ['alarms_and_sirens',
 'baby_crying',
 'bird_singing',
 'bus',
 'cat_meowing',
 'crowd_applause',
 'crowd_cheering',
 'dog_barking',
 'footsteps',
 'glass_smash',
 'gun_shot',
 'horsewalk',
 'mixer',
 'motorcycle',
 'rain',
 'thunder']
batch_size = 32
frame_len = 0.04
seq_len = 2.56
feat_seq_len = int(2.56*50)
num_frames = int(seq_len/frame_len)

thresh = 0.5
vis = visdom.Visdom()

###### Functions ######

def get_file_gt(filenumber,dur):

	total_time_segments = int(dur/frame_len)
	file_labels = np.zeros((total_time_segments, num_classes)) #SECONDS x CLASSES
	filename = annotation_path+"TUT-SED-synthetic-2016-annot-{}.txt".format(filenumber)
	metadata_container = MetaDataContainer().load(filename)
	for num,event in enumerate(classes):
		metadata = metadata_container.filter(event_label=event)
		if len(metadata) == 0:
			file_labels[:,num] = 0
		else:
			for metadata_item in metadata_container:
				onset = int(metadata_item['event_onset']/frame_len)
				offset = int(metadata_item['event_offset']/frame_len)

				file_labels[onset:offset,num] = 1

	return file_labels

def accuracy(output, each_gt):
	time_segments = output.size()[1] / num_classes
	output = output.view(time_segments,num_classes).cpu().data.numpy()
	each_gt = each_gt.view(time_segments,num_classes).cpu().data.numpy()

	output[output>=thresh] = 1
	output[output<thresh] = 0

	print classification_report(each_gt, output, target_names=classes)
	return f1_score(each_gt, output, average='weighted')

def save_checkpoint(state, is_best=False, filename='checkpoint.pth.tar'):
	torch.save(state, filename)
	if is_best:
		shutil.copyfile(filename, 'model_best.pth.tar')

def plot_visdom(x,y,win):
	vis.line(Y=y, X=x, win=win, opts=dict(title=win))

def train_cnn():
	cnn = nnet()
	if torch.cuda.is_available():
		cnn.cuda()
	criterion = nn.BCELoss()
	optimizer = optim.Adam(cnn.parameters())

	running_loss = []
	loss_each_epoch = []
	f1_scores = []
	f1_scores_epoch = []

	for epoch in xrange(epochs):
		print '######################### Epoch {} ###############################'.format(epoch)
		for filenumber in xrange(40,100):
			print "epoch: {}, file: {}".format(epoch,filenumber)
			filename = norm_features_path+"TUT-SED-synthetic-2016-mix-{}.cpickle".format(filenumber)
			feat_norm = pickle.load(open(filename))
			dur = feat_norm.shape[0] / 50
			file_gt = get_file_gt(filenumber,dur)
			
			for i in xrange(feat_seq_len,feat_norm.shape[0],feat_seq_len):
				j = (i/feat_seq_len)*num_frames #variable for GT

				each_batch = feat_norm[i-feat_seq_len:i]
				each_gt = file_gt[j-num_frames:j]

				each_batch = np.asarray(each_batch[np.newaxis, np.newaxis, :, :], dtype=np.float32)
				each_gt =  each_gt.flatten().astype(np.float32)[np.newaxis,:]

				each_batch = torch.from_numpy(each_batch)
				each_gt = torch.from_numpy(each_gt)

				if torch.cuda.is_available():
					each_batch = Variable(each_batch).cuda()
					each_gt = Variable(each_gt).cuda()
				else:
					each_batch = Variable(each_batch)
					each_gt = Variable(each_gt)

				output = cnn(each_batch)
				
				optimizer.zero_grad()
				output = output[:,:each_gt.size()[1]]
				loss = criterion(output, each_gt)
				loss.backward()
				optimizer.step()

				if feat_norm.shape[0]-i < feat_seq_len:
					print "epoch: {}, file: {}, {}/{}".format(epoch, filenumber, i,feat_norm.shape[0])
					f1_scores.append(accuracy(output, each_gt))
			
				running_loss.append(loss.cpu().data.numpy()[0])
				
		loss_each_epoch.append(running_loss[-1])
		f1_scores_epoch.append(f1_scores[-1])
		print "epoch: {}, epoch_end_loss: {}".format(epoch, running_loss[-1])


		# Visualization
		plot_visdom(np.arange(len(running_loss)), np.asarray(running_loss), win='running_loss')
		plot_visdom(np.arange(len(loss_each_epoch)), np.asarray(loss_each_epoch), win='loss_each_epoch')

		plot_visdom(np.arange(len(f1_scores)), np.asarray(f1_scores), win='f1_scores')
		plot_visdom(np.arange(len(f1_scores_epoch)), np.asarray(f1_scores_epoch), win='f1_scores_epoch')

		save_checkpoint({
			'epoch': epoch + 1,
			'arch': "cnn",
			'state_dict': cnn.state_dict(),
			# 'best_prec1': best_prec1,
			'optimizer' : optimizer.state_dict(),
		}) #, is_best

if __name__ == '__main__':
	
	train_cnn()