# README #
* This is an implementation of the [paper](https://arxiv.org/abs/1604.00861) RNN for Polyphonic SED.
* This is still a WIP. The results obtained are sub-optimal. It will be fixed in upcoming weeks.
### Contents ###

* `Model` folders contains different experimental setup, with each containing different experiments within
    * `sed_network.py` contains the network class
    * `sed_train_val.py` contains the training and validation codes
* Dataset being used is `TUT-SED-synthetic-2016` which is mentioned in the parent repo.
* Dependencies
    * PyTorch.
    * repo contains DCASE-framework files.
* `weights` of the Models aren't uploaded due to size constraints, please open an Issue in case you want it :)
