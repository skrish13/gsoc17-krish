import csv, pickle, shutil, visdom
import torch.optim as optim
import numpy as np

from torch.autograd import Variable
from dcase_framework.metadata import MetaDataContainer
from sklearn.metrics import classification_report, f1_score

from sed_network import *

###### PATHS ######
norm_features_path = "../../../TUT-SED-synthetic-2016/features_normalized/"
annotation_path = "../../../TUT-SED-synthetic-2016/meta/"
train_txt = "../../../TUT-SED-synthetic-2016/evaluation_setup/train.txt"


#### PARAMS ####
epochs = 200
num_classes = 16
classes = ['alarms_and_sirens',
 'baby_crying',
 'bird_singing',
 'bus',
 'cat_meowing',
 'crowd_applause',
 'crowd_cheering',
 'dog_barking',
 'footsteps',
 'glass_smash',
 'gun_shot',
 'horsewalk',
 'mixer',
 'motorcycle',
 'rain',
 'thunder']
batch_size = 32
frame_len = 0.04
seq_len = 2.56
feat_seq_len = int(2.56*50)
num_frames = int(seq_len/frame_len)

thresh = 0.5
vis = visdom.Visdom()

###### Functions ######

def get_file_gt(filenumber,dur):

	num_frames_file = int(dur/frame_len)
	file_labels = np.zeros((num_frames_file, num_classes)) #SECONDS x CLASSES
	filename = annotation_path+"TUT-SED-synthetic-2016-annot-{}.txt".format(filenumber)
	metadata_container = MetaDataContainer().load(filename)
	for num,event in enumerate(classes):
		metadata = metadata_container.filter(event_label=event)
		if len(metadata) == 0:
			file_labels[:,num] = 0
		else:
			for metadata_item in metadata_container:
				onset = int(metadata_item['event_onset']/frame_len)
				offset = int(metadata_item['event_offset']/frame_len)

				file_labels[onset:offset,num] = 1

	return file_labels

def accuracy(output, each_gt):
	time_segments = output.size()[1] / num_classes
	output = output.view(time_segments,num_classes).cpu().data.numpy()
	each_gt = each_gt.view(time_segments,num_classes).cpu().data.numpy()

	output[output>=thresh] = 1
	output[output<thresh] = 0

	print classification_report(each_gt, output, target_names=classes)
	return f1_score(each_gt, output, average='weighted')

def accuracy_all(target_labels, output_labels):
	assert len(target_labels) == len(output_labels)
	outputs = np.array([])
	gt = np.array([])
	for index,(each_gt,output) in enumerate(zip(target_labels, output_labels)):
		if index == 0:
			outputs = output
			gt = each_gt
		else:
			outputs = np.concatenate([outputs,output],axis=0)
			gt = np.concatenate([gt,each_gt],axis=0)

	outputs[outputs>=thresh] = 1
	outputs[outputs<thresh] = 0

	print classification_report(gt, outputs, target_names=classes)
	return f1_score(gt, outputs, average='weighted')

def save_checkpoint(state, is_best=False, filename='checkpoint.pth.tar'):
	torch.save(state, filename)
	if is_best:
		shutil.copyfile(filename, 'model_best.pth.tar')

def plot_visdom(x,y,win):
	vis.line(Y=y, X=x, win=win, opts=dict(title=win))

def train_cnn():
	cnn = nnet()
	if torch.cuda.is_available():
		cnn.cuda()
	criterion = nn.BCELoss()
	optimizer = optim.Adam(cnn.parameters())

	running_loss = []
	train_loss_epoch = []
	f1_scores_test = []
	output_labels = []
	target_labels = []

	for epoch in xrange(epochs):
		print '######################### Epoch {} ###############################'.format(epoch)
		temp_loss_epoch = []

		for filenumber in xrange(40,100):
			print "epoch: {}, file: {}".format(epoch,filenumber)
			filename = norm_features_path+"TUT-SED-synthetic-2016-mix-{}.cpickle".format(filenumber)
			feat_norm = pickle.load(open(filename))
			dur = feat_norm.shape[0] / 50
			file_gt = get_file_gt(filenumber,dur)
			
			for i in xrange(feat_seq_len,feat_norm.shape[0],feat_seq_len):
				j = (i/feat_seq_len)*num_frames #variable for GT

				each_batch = feat_norm[i-feat_seq_len:i]
				each_gt = file_gt[j-num_frames:j]

				each_batch = np.asarray(each_batch[np.newaxis, np.newaxis, :, :], dtype=np.float32)
				each_gt =  each_gt.flatten().astype(np.float32)[np.newaxis,:]

				each_batch = torch.from_numpy(each_batch)
				each_gt = torch.from_numpy(each_gt)

				if torch.cuda.is_available():
					each_batch = Variable(each_batch).cuda()
					each_gt = Variable(each_gt).cuda()
				else:
					each_batch = Variable(each_batch)
					each_gt = Variable(each_gt)

				output = cnn(each_batch)
				
				optimizer.zero_grad()
				output = output[:,:each_gt.size()[1]]
				loss = criterion(output, each_gt)
				loss.backward()
				optimizer.step()

				# if feat_norm.shape[0]-i < feat_seq_len:
				# 	print "epoch: {}, file: {}, {}/{}".format(epoch, filenumber, i,feat_norm.shape[0])
				# 	f1_scores.append(accuracy(output, each_gt))	
				
				# print each_gt.cpu().data.numpy().shape # 1x1024 - same for output
				# print each_gt.cpu().data.numpy()[0].shape # 1024,

				time_segments = output.size()[1] / num_classes
				target_labels.append(each_gt.cpu().data.view(time_segments,num_classes).numpy())
				output_labels.append(output.cpu().data.view(time_segments,num_classes).numpy())

				running_loss.append(loss.cpu().data.numpy()[0])
				temp_loss_epoch.append(loss.cpu().data.numpy()[0])
				
		train_loss_epoch.append(sum(temp_loss_epoch) / len(temp_loss_epoch))
		# f1_scores.append(accuracy_all(target_labels, output_labels))
		
		print "epoch: {}, train_loss_mean_epoch: {}".format(epoch, train_loss_epoch)

		# Visualization
		plot_visdom(np.arange(len(running_loss)), np.asarray(running_loss), win='running_loss')
		plot_visdom(np.arange(len(train_loss_epoch)), np.asarray(train_loss_epoch), win='train_loss_epoch')
		# plot_visdom(np.arange(len(f1_scores)), np.asarray(f1_scores), win='f1_scores')


		###########  Validation part ###########
		temp_loss_epoch = []

		for filenumber in xrange(20,40):
			print "epoch: {}, file: {}".format(epoch,filenumber)
			filename = norm_features_path+"TUT-SED-synthetic-2016-mix-{}.cpickle".format(filenumber)
			feat_norm = pickle.load(open(filename))
			dur = feat_norm.shape[0] / 50
			file_gt = get_file_gt(filenumber,dur)
			
			for i in xrange(feat_seq_len,feat_norm.shape[0],feat_seq_len):
				j = (i/feat_seq_len)*num_frames #variable for GT

				each_batch = feat_norm[i-feat_seq_len:i]
				each_gt = file_gt[j-num_frames:j]

				each_batch = np.asarray(each_batch[np.newaxis, np.newaxis, :, :], dtype=np.float32)
				each_gt =  each_gt.flatten().astype(np.float32)[np.newaxis,:]

				each_batch = torch.from_numpy(each_batch)
				each_gt = torch.from_numpy(each_gt)

				if torch.cuda.is_available():
					each_batch = Variable(each_batch).cuda()
					each_gt = Variable(each_gt).cuda()
				else:
					each_batch = Variable(each_batch)
					each_gt = Variable(each_gt)

				output = cnn(each_batch)
				
				optimizer.zero_grad()
				output = output[:,:each_gt.size()[1]]
				loss = criterion(output, each_gt)
				loss.backward()
				optimizer.step()

				# if feat_norm.shape[0]-i < feat_seq_len:
				# 	print "epoch: {}, file: {}, {}/{}".format(epoch, filenumber, i,feat_norm.shape[0])
				# 	f1_scores.append(accuracy(output, each_gt))	
				
				# print each_gt.cpu().data.numpy().shape # 1x1024 - same for output
				# print each_gt.cpu().data.numpy()[0].shape # 1024,

				time_segments = output.size()[1] / num_classes
				target_labels.append(each_gt.cpu().data.view(time_segments,num_classes).numpy())
				output_labels.append(output.cpu().data.view(time_segments,num_classes).numpy())

				running_loss.append(loss.cpu().data.numpy()[0])
				temp_loss_epoch.append(loss.cpu().data.numpy()[0])
				
		train_loss_epoch.append(sum(temp_loss_epoch) / len(temp_loss_epoch))
		f1_scores.append(accuracy_all(target_labels, output_labels))
		
		print "epoch: {}, train_loss_mean_epoch: {}".format(epoch, train_loss_epoch)

		# Visualization
		plot_visdom(np.arange(len(running_loss)), np.asarray(running_loss), win='running_loss')
		plot_visdom(np.arange(len(train_loss_epoch)), np.asarray(train_loss_epoch), win='train_loss_epoch')
		plot_visdom(np.arange(len(f1_scores)), np.asarray(f1_scores), win='f1_scores')


		save_checkpoint({
			'epoch': epoch + 1,
			'arch': "cnn",
			'state_dict': cnn.state_dict(),
			# 'best_prec1': best_prec1,
			'optimizer' : optimizer.state_dict(),
		}) #, is_best

if __name__ == '__main__':
	
	train_cnn()