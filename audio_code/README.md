# README #

### Contents ###

* `audio_api.py` is the main file, which generates the csv output.
* Outputs (for each time-frame):
    * Silence vs non silent 
    * Music vs speech segments
    * SpeakerID 
    * Arousal and Valence scores
* Path to the video needs to be given as command line argument when running the file.
    * `python audio_api.py <path_to_video>`
* Dependencies
    * numpy.
    * repo contains PyAudioAnalysis source files.
* `sed` contains work related to the Sound Event Detection tasks
