# To do task list

- [x] convert vid2aud
- [x] get non-silent and silent parts
- [x] take non-silent and split into music and speech parts
- [x] implement in fusion mode

- [ ] event based segmentation

- [x] speech: Speaker diarization
- [ ] speech: Pitch related differentiation

- [ ] music: important event detection: sudden changes etc
